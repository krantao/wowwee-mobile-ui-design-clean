import { Component, OnInit } from '@angular/core';
import { IonicModule, NavController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-view-driver',
  templateUrl: './view-driver.page.html',
  styleUrls: ['./view-driver.page.scss'],
})
export class ViewDriverPage implements OnInit {

  constructor(public navCtrl: NavController, public actionSheetCtrl: ActionSheetController) { }

  ngOnInit() {
  }

}
