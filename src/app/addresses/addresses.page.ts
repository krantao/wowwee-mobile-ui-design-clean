import { Component, OnInit } from '@angular/core';
import { IonicModule, NavController, IonApp, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-addresses',
  templateUrl: './addresses.page.html',
  styleUrls: ['./addresses.page.scss'],
})
export class AddressesPage implements OnInit {

  constructor(public navCtrl: NavController, public myApp: IonApp, public menuCtrl: MenuController) { }

  items = [
    {addressType: 'Primary', complex: 'Waterfall', street: '14 Bandana Turn', Town: 'Midrand', Province: 'Gauteng'},
    {addressType: 'Office 1', complex: 'Ferndale Park', street: '113 Sloane Street', Town: 'Ferndale', Province: 'Gauteng'},
    {addressType: 'Office 2', complex: 'Randburg Mids', street: '2 Sloppy Cul', Town: 'Randburg', Province: 'Gauteng'},
    {addressType: 'Office 3', complex: 'Sandton Sqaure', street: '224 Prince Drive', Town: 'Sandton', Province: 'Gauteng'},
    {addressType: 'Office 4', complex: 'Windsor Tower', street: '887 Tax Turn', Town: 'Windsor East', Province: 'Gauteng'},
  ];

  ngOnInit() {
  }

}
