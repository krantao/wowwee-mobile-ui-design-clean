import { Component, OnInit } from '@angular/core';
import { IonicModule, NavController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-add-vehicle',
  templateUrl: './add-vehicle.page.html',
  styleUrls: ['./add-vehicle.page.scss'],
})
export class AddVehiclePage implements OnInit {

  img = '';
  consumption;
  fuel;
  distance;
  driver;

  constructor(public navCtrl: NavController, public actionSheetCtrl: ActionSheetController) { }

  ngOnInit() {
  }

}
