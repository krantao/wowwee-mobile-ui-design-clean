import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewCompanyPageRoutingModule } from './view-company-routing.module';

import { ViewCompanyPage } from './view-company.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewCompanyPageRoutingModule
  ],
  declarations: [ViewCompanyPage]
})
export class ViewCompanyPageModule {}
