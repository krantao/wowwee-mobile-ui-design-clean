import { Component, OnInit } from '@angular/core';
import { IonicModule, NavController, IonApp, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-view-company',
  templateUrl: './view-company.page.html',
  styleUrls: ['./view-company.page.scss'],
})
export class ViewCompanyPage implements OnInit {

  constructor(public navCtrl: NavController, public myApp: IonApp, public menuCtrl: MenuController) { }

  items = [
    {name: 'DocubleClick', parent: 'Midas Pty', createdBy: 'Sam Typist', created: '2019/12/13'},
  ];

  ngOnInit() {
  }

}
