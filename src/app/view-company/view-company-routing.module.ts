import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewCompanyPage } from './view-company.page';

const routes: Routes = [
  {
    path: '',
    component: ViewCompanyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewCompanyPageRoutingModule {}
