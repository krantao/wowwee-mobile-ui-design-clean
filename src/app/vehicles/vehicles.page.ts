import { Component, OnInit } from '@angular/core';
import { IonicModule, NavController, IonApp, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.page.html',
  styleUrls: ['./vehicles.page.scss'],
})
export class VehiclesPage implements OnInit {
  buttonColor = 'secondary';

  constructor(public navCtrl: NavController, public myApp: IonApp, public menuCtrl: MenuController) { }


  items = [
    {manufacturer: 'Corsa', model: 'Utility', registration: 'FF 65 NC GP', img: 'assets/imgs/corsa_utility.jpg', vehicleStatus: 'Order Tag', buttonColor: "secondary"},
    {manufacturer: 'Hyundai', model: '1 Ton', registration: 'HJ 43 BY GP', img: 'assets/imgs/hyundai_1ton.jpg', vehicleStatus: 'Link', buttonColor: "primary"},
    {manufacturer: 'Mercedes', model: 'X Class', registration: 'BP 22 BB GP', img: 'assets/imgs/merc_x.jpg', vehicleStatus: 'Deactivate', buttonColor: "secondary"},
    {manufacturer: 'VW', model: 'Caddy', registration: 'JG 76 HT GP', img: 'assets/imgs/vw_caddy.jpg', vehicleStatus: 'Activate', buttonColor: "secondary"},
    {manufacturer: 'Toyota', model: 'Hilux', registration: 'CC 65 JP GP', img: 'assets/imgs/toyota_hilux.jpg', vehicleStatus: 'Link', buttonColor: "primary"},
  ];

  ngOnInit() {
  }

}
