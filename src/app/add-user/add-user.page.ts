import { Component, OnInit } from '@angular/core';
import { IonicModule, NavController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.page.html',
  styleUrls: ['./add-user.page.scss'],
})
export class AddUserPage implements OnInit {

  company;

  constructor(public navCtrl: NavController, public actionSheetCtrl: ActionSheetController) { }

  ngOnInit() {
  }

}
