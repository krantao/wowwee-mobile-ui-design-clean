import { Component, OnInit } from '@angular/core';
import { IonicModule, NavController, IonApp, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.page.html',
  styleUrls: ['./drivers.page.scss'],
})
export class DriversPage implements OnInit {

  constructor(public navCtrl: NavController, public myApp: IonApp, public menuCtrl: MenuController) { }

  items = [
    {name: 'Lebron', surname: 'James', status: 'Active'},
    {name: 'Solly', surname: 'Molly', status: 'Active'},
    {name: 'Shawn', surname: 'Mendes', status: 'Inactive'},
  ];

  ngOnInit() {
  }

}
