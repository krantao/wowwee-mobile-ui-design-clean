import { Component, OnInit } from '@angular/core';
import { IonicModule, NavController, ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-add-driver',
  templateUrl: './add-driver.page.html',
  styleUrls: ['./add-driver.page.scss'],
})
export class AddDriverPage implements OnInit {

  User;

  constructor(public navCtrl: NavController, public actionSheetCtrl: ActionSheetController) { }

  ngOnInit() {
  }

}
