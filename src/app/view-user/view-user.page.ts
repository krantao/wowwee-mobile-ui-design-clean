import { Component, OnInit } from '@angular/core';
import { IonicModule, NavController, IonApp, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.page.html',
  styleUrls: ['./view-user.page.scss'],
})
export class ViewUserPage implements OnInit {

  constructor(public navCtrl: NavController, public myApp: IonApp, public menuCtrl: MenuController) { }

  items = [
    {name: 'Drake', surname: 'Andrelious', company: 'DHL'},
  ];

  ngOnInit() {
  }

}
