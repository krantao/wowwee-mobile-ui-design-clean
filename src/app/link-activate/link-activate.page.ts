import { Component, OnInit } from '@angular/core';
import { IonicModule, NavController, IonApp, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-link-activate',
  templateUrl: './link-activate.page.html',
  styleUrls: ['./link-activate.page.scss'],
})
export class LinkActivatePage implements OnInit {

  constructor(public navCtrl: NavController, public myApp: IonApp, public menuCtrl: MenuController) { }

  items = [
    {manufacturer: 'Corsa', make: 'Utility', model: '2.0', registration: 'FF 65 NC GP', driver: 'Percy Style', color: 'red', tare: "500"},
  ];

  ngOnInit() {
  }

}
