import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LinkActivatePageRoutingModule } from './link-activate-routing.module';

import { LinkActivatePage } from './link-activate.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LinkActivatePageRoutingModule
  ],
  declarations: [LinkActivatePage]
})
export class LinkActivatePageModule {}
