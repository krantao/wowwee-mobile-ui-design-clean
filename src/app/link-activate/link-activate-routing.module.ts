import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LinkActivatePage } from './link-activate.page';

const routes: Routes = [
  {
    path: '',
    component: LinkActivatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LinkActivatePageRoutingModule {}
