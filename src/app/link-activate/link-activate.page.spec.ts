import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LinkActivatePage } from './link-activate.page';

describe('LinkActivatePage', () => {
  let component: LinkActivatePage;
  let fixture: ComponentFixture<LinkActivatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkActivatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LinkActivatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
