import { Component, OnInit } from '@angular/core';
import { IonicModule, NavController, IonApp, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.page.html',
  styleUrls: ['./companies.page.scss'],
})
export class CompaniesPage implements OnInit {

  constructor(public navCtrl: NavController, public myApp: IonApp, public menuCtrl: MenuController) { }

  items = [
    {name: 'Kellogs'},
    {name: 'Tiger Brand'},
    {name: 'Midas Pty Ltd'},
    {name: 'Carpet Master'},
    {name: 'PEP'},
    {name: 'Mr Price'},
  ];

  ngOnInit() {
  }

}
