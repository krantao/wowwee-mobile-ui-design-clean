import { Component, OnInit } from '@angular/core';
import { IonicModule, NavController, IonApp, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {

  constructor(public navCtrl: NavController, public myApp: IonApp, public menuCtrl: MenuController) { }

  items = [
    {name: 'Lebron', surname: 'James', company: 'PEP'},
    {name: 'Solly', surname: 'Molly', company: 'Edgars'},
    {name: 'Shawn', surname: 'Mendes', company: 'GALLO'},

  ];

  ngOnInit() {
  }

}
