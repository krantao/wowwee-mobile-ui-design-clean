import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-home',
  templateUrl: './new-home.page.html',
  styleUrls: ['./new-home.page.scss'],
})
export class NewHomePage implements OnInit {

  segmentChanged(ev: any) {
    console.log('Segment changed', ev);
  }

  constructor() { }

  ngOnInit() {
  }

}
